#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "point.h"
#include <QPainter>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    event;
    QPainter painter(this);
    for(int i = 0; i < n; i++)
    {
        if(i >= 5) points[i]->setWidth(2);
        points[i]->setSize(i + 3);
        points[i]->draw(&painter);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Point *b = nullptr;
    if(n>=10) return;
    if (event->modifiers() & Qt::ShiftModifier)
        qDebug("ShiftModifier");
    else if (event->modifiers() & Qt::ControlModifier)
        qDebug("ControlModifier");
    else{
        qDebug("x=%d, y=%d",event->x(), event->y());
        b = new Point(event->x(),event->y());
    }
    if(b)
        points[n++] = b;
    repaint();
}


